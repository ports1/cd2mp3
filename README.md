# cd2mp3 README
Ver 0.83 - 2021/06/08
==

CD2MP3 - Written by Jamie Heckford, Development and Maintenance by Chris Hutchinson

Uses lame and dagrab (freeware/GPL)

This program is free for use however...  

Introduction
------------

cd2mp3 takes the hassle away from trying to remember dagrab and lame command line switches,
and having to go through the process of having to use 2 programs to make an mp3. cd2mp3 brings
up a small set of prompts asking you how you would like the mp3 created. Answering them takes
about 10 seconds, and then cd2mp3 will encode the mp3/cd/tracks for you.

Changes since 0.81
------------------

* Code cleanup


Requirements
------------

cd2mp3 requires the following:

Perl 5 (minimum) Installed as /usr/bin/perl (see troubleshooting if it is elsewhere)

LAME installed in your path

dagrab installed in your path 


LAME and Dagrab can both be found somewhere around http://www.blackh0le.net/cd2mp3

Installation
------------

Change to the directory you unpacked the gzipped file to and run (as root): ./install
This will place the program in /usr/local/bin and clean up the temp directory.

If you can't get root access then copy the cd2mp3 binary/script to where you want.

F.A.Q
-----

1. Where can I obtain the most resent version from??
1.a https://gitlab.com/ports1/cd2mp3/

2. Do I have to pay for it?
2.a Nope, cd2mp3 is freeware.

3. Can I modify the source?
3.a Copying is the biggest form of flattery. Seriously, feel free to.. Just leave my name in it ;) 

4. I hate the program.
4.a Remove it then. 

Troubleshooting
---------------

1. Perl is not installed as `/usr/bin/perl`

**Solution:**

Open up the cd2mp3 file (in `vi` or similar). It is installed in `/usr/local/bin/cd2mp3`. Change
the first line `#!/usr/bin/perl` to: `#!/path/to/perl/on/your/system`

2. I don't have LAME or dagrab.
 
**Solution:**

You can obtain the latest versions from our GitLab account at https://gitlab.com/ports1/cd2mp3

3. It doesn't work!!

**Solution:**

Please file any bug reports (including the error message if given and information on your machine) to: https://gitlab.com/ports1/cd2mp3
